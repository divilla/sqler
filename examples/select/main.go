package main

import (
	"fmt"

	"gitlab.com/divilla/sqler"
	"gitlab.com/divilla/sqler/r"
	"gitlab.com/divilla/sqler/s"
)

func main() {
	statement := sqler.NewSelect(r.Taxonomy.Reference_).
		SelectModifiers(s.DISTINCT, s.DISTINCTROW).
		Columns().
		Build()

	fmt.Println(statement)
}
