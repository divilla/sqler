package r

import "gitlab.com/divilla/sqler"

var Taxonomy = struct {
	ID         sqler.ExprFunc
	Name       sqler.ExprFunc
	Slug       sqler.ExprFunc
	Reference_ sqler.ExprFunc
}{
	ID:         func(_ ...string) string { return "id" },
	Name:       func(_ ...string) string { return "name" },
	Slug:       func(_ ...string) string { return "slug" },
	Reference_: func(_ ...string) string { return "taxonomy" },
}
