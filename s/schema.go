package s

import "gitlab.com/divilla/sqler"

var (
	ALL         = sqler.SelectModFunc(func() string { return "ALL" })
	DISTINCT    = sqler.SelectModFunc(func() string { return "DISTINCT" })
	DISTINCTROW = sqler.SelectModFunc(func() string { return "ALL" })
)

func AS(expr string, name string) string {
	return expr + " AS " + name
}

func COUNT(expr ...string) string {
	if len(expr) == 0 {
		return "COUNT(*)"
	}

	return "COUNT(" + expr[0] + ")"
}
