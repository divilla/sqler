package sqler

import (
	"strings"
)

type (
	statement struct {
		name      string
		reference ExprFunc
		sb        *strings.Builder
	}

	Select struct {
		statement
		selectModifiers []SelectModFunc
		columns         []string
	}

	SelectModFunc func() string
	ExprFunc      func(...string) string
)

func newBaseStatement(name string, reference ExprFunc) statement {
	return statement{
		name:      name,
		reference: reference,
		sb:        new(strings.Builder),
	}
}

func NewSelect(reference ExprFunc) *Select {
	s := &Select{
		statement: newBaseStatement("SELECT", reference),
	}
	s.columns = []string{"*"}
	return s
}

func (s *Select) SelectModifiers(smf ...SelectModFunc) *Select {
	s.selectModifiers = smf
	return s
}

func (s *Select) Columns(cols ...string) *Select {
	if len(cols) > 0 {
		s.columns = cols
	}
	return s
}

func (s *Select) Build() string {
	s.sb.WriteString(s.name)
	for _, sm := range s.selectModifiers {
		s.sb.WriteString(" ")
		s.sb.WriteString(sm())
	}
	for k, col := range s.columns {
		if k != 0 {
			s.sb.WriteString(",")
		}
		s.sb.WriteString(" ")
		s.sb.WriteString(col)
	}
	s.sb.WriteString(" FROM ")
	s.sb.WriteString(s.reference())

	return s.sb.String()
}
